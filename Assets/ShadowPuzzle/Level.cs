﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    [SerializeField] GameObject player;
    [SerializeField] GameObject block;

    int[,] blocks;
    const int SIZE = 10;

	void Start()
    {
        blocks = new int[SIZE, SIZE];

        CreatePlayer(2, 1);

        CreateBlock(1, 0, 1);
        CreateBlock(2, 0, 1);
        CreateBlock(3, 0, 1);

        CreateBlock(0, 1, 3);
        CreateBlock(0, 2, 3);
        CreateBlock(0, 3, 3);

        CreateBlock(4, 1, 3);
        CreateBlock(4, 2, 3);
        CreateBlock(4, 3, 3);

        CreateBlock(1, 4, 1);
        CreateBlock(1, 5, 1);
        CreateBlock(1, 6, 1);

        CreateBlock(3, 4, 1);
        CreateBlock(3, 5, 1);
        CreateBlock(3, 6, 1);
	}

    void CreatePlayer(int x, int y)
    {
        Instantiate(player, new Vector3(x * 1.5f, 0f, y * 1.5f), player.transform.rotation);
    }

    void CreateBlock(int x, int y, int depth)
    {
        for (int i = 0; i < depth; i++)
        {
            Instantiate(block, new Vector3(x * 1.5f, blocks[x, y] * 1.5f, y * 1.5f), block.transform.rotation);
            blocks[x, y]++;
        }
    }
}
