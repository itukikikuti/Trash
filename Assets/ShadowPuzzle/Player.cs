﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float size;
    public float second;

    IEnumerator Start()
    {
        while (true)
        {
            if (Input.GetKey(KeyCode.W))
            {
                yield return Move(0, 1);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                yield return Move(0, -1);

            }
            else if (Input.GetKey(KeyCode.A))
            {
                yield return Move(-1, 0);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                yield return Move(1, 0);
            }

            yield return null;
        }
	}

    IEnumerator Move(int x, int y)
    {
        Vector3 origin = transform.position;
        Vector3 target = transform.position + new Vector3(x * size, 0f, y * size);
        float t = 0f;

        while (t <= second)
        {
            transform.position = Vector3.Lerp(origin, target, t / second);
            t += Time.deltaTime;
            yield return null;
        }

        transform.position = target;
    }
}
