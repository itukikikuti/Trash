﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class San : MonoBehaviour
{
    public float speed = 0.5f;
    float targetAngle;

    void Start()
    {
        targetAngle = transform.eulerAngles.y;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            targetAngle = 0f;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            targetAngle = 90f;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            targetAngle = 180f;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            targetAngle = 270f;
        }

        if (Mathf.DeltaAngle(transform.eulerAngles.y, targetAngle) > 100)
        {
            targetAngle -= 180f;
        }

        float angle = Mathf.LerpAngle(transform.eulerAngles.y, targetAngle, speed);
        transform.eulerAngles = new Vector3(45f, angle, 0f);
    }
}
