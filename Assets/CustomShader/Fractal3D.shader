﻿Shader "CustomShader/Fractal3D"
{
	Properties
	{
		_Param("Param", Vector) = (0.0, 0.0, 0.0, 0.0)
		_Test("Test", Float) = 0.0
	}
	SubShader
	{
		Pass
		{
			Cull Off ZWrite Off ZTest Always

			CGPROGRAM
			#pragma vertex Vert
			#pragma fragment Frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f Vert(appdata input)
			{
				v2f output;
				output.vertex = UnityObjectToClipPos(input.vertex);
				output.uv = input.uv;
				return output;
			}
			
			float4 _Param;
			float _Test;

			float3 sphericalharmonic(float3 n)
			{
				float4 c[7];
				c[0] = float4(0.0, 0.5, 0.0, 0.4);
				c[1] = float4(0.0, 0.3, 0.05, 0.45);
				c[2] = float4(0.0, 0.3, -0.3, 0.85);
				c[3] = float4(0.0, 0.2, 0.1, 0.0);
				c[4] = float4(0.0, 0.2, 0.1, 0.0);
				c[5] = float4(0.1, 0.1, 0.1, 0.0);
				c[6] = float4(0.0, 0.0, 0.0, 0.0);
	
				float4 p = float4(n, 1.0);

				float3 l1;
				l1.r = dot(c[0], p);
				l1.g = dot(c[1], p);
				l1.b = dot(c[2], p);

				float4 m2 = p.xyzz * p.yzzx;
				float3 l2;
				l2.r = dot(c[3], m2);
				l2.g = dot(c[4], m2);
				l2.b = dot(c[5], m2);

				float m3 = p.x * p.x - p.y * p.y;
				float3 l3;
				l3 = c[6].xyz * m3;

				float3 sh = float3(l1 + l2 + l3);

				return saturate(sh);
			}

			float2x2 rmat(float t)
			{
				float c = cos(t);
				float s = sin(t);
				return float2x2(c, s, -s, c);
			}

			float3x3 rmat(float3 r)
			{
				float3 a = float3(cos(r.x) * cos(r.y), sin(r.y), sin(r.x) * cos(r.y));
				
				float c = cos(r.z);
				float s = sin(r.z);
				float3 as = a * s;
				float3 ac = a * a * (1.0 - c);
				float3 ad = a.yzx * a.zxy * (1.0 - c);
	
				return float3x3(
					c + ac.x, 
					ad.z - as.z, 
					ad.y + as.y,
					ad.z + as.z, 
					c + ac.y, 
					ad.x - as.x,
					ad.y - as.y, 
					ad.x + as.x, 
					c + ac.z);
			}

			float3 axis()
			{
				return normalize(abs(float3(cos(_Param.x), 2.0 / _Param.x - 2.0 / _Param.y, sin(_Param.y)))) * 4.0;
			}
			
			float3 global_color;
			float3x3 rotation;

			float map(float3 p)
			{
				float3 o = float3(1.0, 1.5, 5.0);
				p -= o;
				p.xz = mul(p.xz, rmat(_Time.x * 0.0125));

				float s = 1.5;
				const int it = 16;
				float u = pow(s, -float(it) - 1.0);
				global_color;
	
				for (int i = 0; i < it; i++) 
				{
					p = abs(mul(p, rotation)) * s - axis() * 5.0;
					global_color += cos(p * 2.0);
				}

				return max(max(p.x, p.y), p.z) * u;
			}

			float curv2(in float3 p, in float w)
			{
				float3 e = float3(w, 0.0, 0.0);
	
				float t1 = map(p + e.xyy), t2 = map(p - e.xyy);
				float t3 = map(p + e.yxy), t4 = map(p - e.yxy);
				float t5 = map(p + e.yyx), t6 = map(p - e.yyx);
	
				return 0.25 / e.x * (t1 + t2 + t3 + t4 + t5 + t6 - 6.0 * map(p));
			}

			float3 derive(const in float3 position, const in float epsilon)
			{
				float2 offset = float2(epsilon, -epsilon);
				float4 simplex;
				simplex.x = map(position + offset.xyy);
				simplex.y = map(position + offset.yyx);
				simplex.z = map(position + offset.yxy);
				simplex.w = map(position + offset.xxx);
	
				float3 normal = offset.xyy * simplex.x + offset.yyx * simplex.y + offset.yxy * simplex.z + offset.xxx * simplex.w;
				return normalize(normal);
			}

			fixed4 Frag(v2f input) : SV_Target
			{
				float2 uv = input.uv.xy / _ScreenParams.xy;
				uv = uv * 2.0 - 1.0;
				uv *= _ScreenParams.xy / _ScreenParams.yy;
	
				float3 origin = float3(0.0, 1.25, 0.0);
				float3 position = origin;
				float fov = 1.1;
				float3 direction = normalize(normalize(float3(uv, fov)));
	
				rotation = rmat(axis());
	
				float threshold = 1.0 / min(_ScreenParams.x, _ScreenParams.y);
				float magnitude = threshold;
				float range = 0.0;
				float steps = 0.0;

				float exponent = 0.95;
				float epsilon = threshold;
				const float max_range = 16.0;
				const int iterations = 128;

				for (int i = 0; i < iterations; i++)
				{
					if (range < max_range) 
					{
						if (magnitude >= threshold)
						{
							magnitude = map(position);
							range += magnitude * 0.8;
							threshold *= 1.0 + 0.0025 * steps;
			
							epsilon = 0.5 * (pow(range / min(_ScreenParams.x, _ScreenParams.y), exponent));
							exponent -= threshold;
							position = origin + direction * range;
	
							steps++;
						}
					}
				}
	
				range = distance(origin, position);
				epsilon = pow(range / min(_ScreenParams.x, _ScreenParams.y), exponent);
	
	
				float3 normal;
				float3 color;
	
				if (range < max_range)
				{
					float fog = 0.5 * saturate(-exp2(range)) - abs(steps / float(iterations + 1));
					float light = 1.0;
					float3 gradient;
					float curvature = curv2(position, epsilon * 8.0);
					curvature += curv2(position, epsilon * 4.0 * (1.0 - curvature));

					gradient = derive(position, 2.0 * epsilon + curvature);
					position += gradient * epsilon;
					gradient = derive(position, epsilon);
		
					normal = normalize(gradient);

					light = clamp(dot(normal, normalize(float3(4.0, 32.0, 12.0))), 0.0, 0.25);

					color = 1.0 - lerp(sphericalharmonic(-normal), sphericalharmonic(reflect(direction, normalize(1.0 - normal))), max(1.0 - cos(dot(direction, normal)), 0.0));
					color = lerp(color, normalize(global_color) * 0.5 + 0.5, 0.5);
					color *= 0.5 + fog + curvature;
					color += light;
				}
				else
				{
					normal = normalize(position);
					color = sphericalharmonic(normal);
				}
	
				return float4(color, 1.0);
			}
			ENDCG
		}
	}
}
