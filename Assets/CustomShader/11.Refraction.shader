﻿Shader "CustomShader/11.Refraction" {
	Properties {
		_eta("ETA", Range(0, 1)) = 0.8
		_cubeMap("Cube Map", Cube) = "white" {}
	}
	SubShader {
		Tags {
			"RenderType" = "Opaque"
		}

		Pass {
			CGPROGRAM
			#pragma vertex Vert
			#pragma fragment Frag
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			
			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
				float3 viewDirection : TEXCOORD0;
			};

			float _eta;
			samplerCUBE _cubeMap;

			fixed4 Refraction(samplerCUBE cubeMap, float3 viewDirection, float3 normal) {
				return texCUBE(cubeMap, refract(viewDirection, normal, _eta));
			}

			v2f Vert(appdata input) {
				v2f output;
				output.vertex = UnityObjectToClipPos(input.vertex);
				output.normal = UnityObjectToWorldNormal(input.normal);
				output.viewDirection = mul(unity_ObjectToWorld, input.vertex).xyz - _WorldSpaceCameraPos;
				return output;
			}

			fixed4 Frag(v2f input) : SV_Target {
				float3 normal = normalize(input.normal);

				return Refraction(_cubeMap, input.viewDirection, normal);
			}
			ENDCG
		}
	}
}
