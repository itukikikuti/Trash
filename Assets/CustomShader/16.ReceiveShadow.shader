﻿Shader "CustomShader/16.ReceiveShadow" {
	Properties {
	}
	SubShader {
		Pass {
			Tags {
				"LightMode" = "ForwardBase"
			}
			CGPROGRAM
			#pragma vertex Vert
			#pragma fragment Frag
			#pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			
			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
				float4 shadowCoord : TEXCOORD0;
			};

			v2f Vert(appdata input) {
				v2f output;
				output.vertex = UnityObjectToClipPos(input.vertex);
				output.normal = UnityObjectToWorldNormal(input.normal);
				output.shadowCoord = ComputeScreenPos(output.vertex);
				return output;
			}

			fixed4 Lambert(float3 normal) {
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float lightIntensity = saturate(dot(normal, lightDirection));

				return saturate(_LightColor0 * lightIntensity);
			}

			UNITY_DECLARE_SCREENSPACE_SHADOWMAP(_ShadowMapTexture);

			fixed4 Frag(v2f input) : SV_Target {
				float3 normal = normalize(input.normal);
				float shadow = UNITY_SAMPLE_SCREEN_SHADOW(_ShadowMapTexture, input.shadowCoord);

				return Lambert(normal) * shadow;
			}
			ENDCG
		}

		Pass {
			Tags {
				"LightMode" = "ShadowCaster"
			}

			CGPROGRAM
			#pragma vertex Vert
			#pragma fragment Frag
			#pragma multi_compile_shadowcaster
			#include "UnityCG.cginc"
			
			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
			};

			v2f Vert(appdata input) {
				v2f output;
				output.vertex = UnityClipSpaceShadowCasterPos(input.vertex, input.normal);
				output.vertex = UnityApplyLinearShadowBias(output.vertex);
				return output;
			}

			fixed4 Frag(v2f input) : SV_Target {
				return fixed4(0, 0, 0, 0);
			}
			ENDCG
		}
	}
}
