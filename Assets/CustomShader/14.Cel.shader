﻿Shader "CustomShader/14.Cel" {
	Properties {
		_thickness("Thickness", Range(0.001, 0.01)) = 0.0025
		_rampTexture("Ramp Texture", 2D) = "white" {}
		_ditherMask("Dither Mask", 2D) = "white" {}
	}
	SubShader {
		Tags {
			"RenderType" = "Opaque"
		}

		Pass {
			CGPROGRAM
			#pragma vertex Vert
			#pragma fragment Frag
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			
			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
				float3 viewNormal : TEXCOORD0;
				float4 screenPosition : TEXCOORD1;
			};

			sampler2D _rampTexture;
			sampler2D _ditherMask;
			
			fixed4 Cel(float3 normal) {
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float lightIntensity = saturate(dot(normal, lightDirection));

				fixed4 ramp = tex2D(_rampTexture, float2(lightIntensity, 0.5));
				return saturate(_LightColor0 * ramp);
			}
			
			v2f Vert(appdata input) {
				v2f output;
				output.vertex = UnityObjectToClipPos(input.vertex);
				output.normal = UnityObjectToWorldNormal(input.normal);
				output.viewNormal = normalize(mul((float3x3)UNITY_MATRIX_MV, input.normal));
				output.screenPosition = ComputeScreenPos(output.vertex);
				return output;
			}

			fixed4 Frag(v2f input) : SV_Target {
                float3 normal = normalize(input.normal);

                float light = Luminance(Cel(normal));

				float a = Luminance(tex2D(_ditherMask, float2(input.screenPosition.xy / input.screenPosition.w * _ScreenParams.xy / 4)));
				if (light > a)
					return fixed4(1, 1, 1, 1);
				
				return fixed4(0, 0, 0, 1);
			}
			ENDCG
		}

		Pass {
			Cull Front
			CGPROGRAM
			#pragma vertex Vert
			#pragma fragment Frag
			#include "UnityCG.cginc"
			
			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
			};

			float _thickness;

			v2f Vert(appdata input) {
				v2f output;
				output.vertex = UnityObjectToClipPos(input.vertex);
				float3 normal = UnityObjectToWorldNormal(input.normal);
				float2 offset = TransformViewToProjection(normal.xy);
				output.vertex.xy += offset * output.vertex.z * _thickness;
				return output;
			}

			fixed4 Frag(v2f input) : SV_Target {
				return fixed4(0, 0, 0, 1);
			}
			ENDCG
		}
	}
}
