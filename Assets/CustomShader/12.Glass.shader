﻿Shader "CustomShader/12.Glass" {
	Properties {
		_shininess("Shininess", Float) = 100
		_bias("Bias", Range(0, 8)) = 1
		_eta("ETA", Range(0, 1)) = 0.7
		_abberation("Abberation", Range(0, 0.5)) = 0.05
		_cubeMap("Cube Map", Cube) = "white" {}
		_bumpMap("Bump Map", 2D) = "white" {}
	}
	SubShader {
		Tags {
			"RenderType" = "Opaque"
		}

		Pass {
			CGPROGRAM
			#pragma vertex Vert
			#pragma fragment Frag
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			
			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 tangent : TANGENT;
				float2 texcoord : TEXCOORD;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
				float2 texcoord : TEXCOORD0;
				float3 viewDirection : TEXCOORD1;
				float3 viewNormal : TEXCOORD2;
				float3 tspace0 : TEXCOORD3;
				float3 tspace1 : TEXCOORD4;
				float3 tspace2 : TEXCOORD5;
			};

			float _shininess;
			float _bias;
			float _eta;
			float _abberation;
			samplerCUBE _cubeMap;
			sampler2D _bumpMap;
			
			fixed4 Specular(float3 normal, float3 viewDirection) {
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float3 v = normalize(viewDirection);
				float3 r = reflect(lightDirection, normal);
				float specularIntensity = pow(saturate(dot(v, r)), _shininess);

				return saturate(_LightColor0 * specularIntensity);
			}

			fixed4 Reflection(samplerCUBE cubeMap, float3 viewDirection, float3 normal) {
				return texCUBElod(cubeMap, float4(reflect(viewDirection, normal), _bias));
			}

			fixed4 RefractionByChromaticAbberation(samplerCUBE cubeMap, float3 viewDirection, float3 viewNormal, float3 normal) {
				fixed4 output;
				output.r = texCUBElod(cubeMap, float4(refract(viewDirection, normal, _eta + _abberation), _bias)).r;
				output.g = texCUBElod(cubeMap, float4(refract(viewDirection, normal, _eta), _bias)).g;
				output.b = texCUBElod(cubeMap, float4(refract(viewDirection, normal, _eta - _abberation), _bias)).b;
				output.a = 1;
				return output;
			}

			v2f Vert(appdata input) {
				v2f output;
				output.vertex = UnityObjectToClipPos(input.vertex);
				output.normal = UnityObjectToWorldNormal(input.normal);
				output.texcoord = input.texcoord;
				output.viewDirection = mul(unity_ObjectToWorld, input.vertex).xyz - _WorldSpaceCameraPos;
				output.viewNormal = normalize(mul((float3x3)UNITY_MATRIX_MV, input.normal));

				float3 tangent = UnityObjectToWorldDir(input.tangent.xyz);
				float tangentSign = input.tangent.w * unity_WorldTransformParams.w;
				float3 bitangent = cross(output.normal, tangent) * tangentSign;
				output.tspace0 = float3(tangent.x, bitangent.x, output.normal.x);
				output.tspace1 = float3(tangent.y, bitangent.y, output.normal.y);
				output.tspace2 = float3(tangent.z, bitangent.z, output.normal.z);

				return output;
			}

			fixed4 Frag(v2f input) : SV_Target {
				float3 normal;
				float3 normalMap = UnpackNormal(tex2D(_bumpMap, input.texcoord));
                normal.x = dot(input.tspace0, normalMap);
                normal.y = dot(input.tspace1, normalMap);
                normal.z = dot(input.tspace2, normalMap);
                normal = normalize(normal);
				float3 viewNormal = normalize(input.viewNormal);
				float v = 1 - viewNormal.z;

				fixed4 refraction = RefractionByChromaticAbberation(_cubeMap, input.viewDirection, viewNormal, normal);
				fixed4 reflection = Reflection(_cubeMap, input.viewDirection, normal) * v;
				fixed4 light = Specular(normal, input.viewDirection);
				
				return refraction + reflection + light;
			}
			ENDCG
		}
	}
}
