﻿Shader "CustomShader/10.Wireframe" {
	Properties {
		_color("Color", Color) = (1, 1, 1, 1)
	}
	SubShader {
		Tags {
			"RenderType" = "Transparent"
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
		}

		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			Cull Off

			CGPROGRAM
			#pragma vertex Vert
			#pragma geometry Geom
			#pragma fragment Frag
			#include "UnityCG.cginc"

			struct appdata {
				float4 vertex : POSITION;
			};

			struct v2g {
				float4 vertex : SV_POSITION;
				float4 position : TEXCOORD0;
			};

			struct g2f {
				float4 vertex : SV_POSITION;
				float4 position : TEXCOORD0;
				float4 distance : TEXCOORD1;
			};

			fixed4 _color;
			
			v2g Vert(appdata input) {
				v2g output;
				output.vertex = UnityObjectToClipPos(input.vertex);
				output.position = mul(unity_ObjectToWorld, input.vertex);
				return output;
			}
			
			[maxvertexcount(3)]
			void Geom(triangle v2g input[3], inout TriangleStream<g2f> triangleStream) {
				float2 p0 = input[0].vertex.xy / input[0].vertex.w;
				float2 p1 = input[1].vertex.xy / input[1].vertex.w;
				float2 p2 = input[2].vertex.xy / input[2].vertex.w;
				float2 edge0 = p2 - p1;
				float2 edge1 = p2 - p0;
				float2 edge2 = p1 - p0;

				float area = abs(edge1.x * edge2.y - edge1.y * edge2.x);
				float thickness = 500;

				g2f output;

				output.vertex = input[0].vertex;
				output.position = input[0].position;
				output.distance.xyz = float3(area / length(edge0), 0, 0) * output.vertex.w * thickness;
				output.distance.w = 1 / output.vertex.w;
				triangleStream.Append(output);

				output.vertex = input[1].vertex;
				output.position = input[1].position;
				output.distance.xyz = float3(0, area / length(edge1), 0) * output.vertex.w * thickness;
				output.distance.w = 1 / output.vertex.w;
				triangleStream.Append(output);

				output.vertex = input[2].vertex;
				output.position = input[2].position;
				output.distance.xyz = float3(0, 0, area / length(edge2)) * output.vertex.w * thickness;
				output.distance.w = 1 / output.vertex.w;
				triangleStream.Append(output);
			}

			fixed4 Frag (g2f input) : SV_Target {
				float minDistance = min(input.distance.x, min(input.distance.y, input.distance.z)) * input.distance.w;

				float t = exp2(-2 * minDistance * minDistance);
				return lerp(fixed4(0, 0, 0, 0), _color, t);
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
