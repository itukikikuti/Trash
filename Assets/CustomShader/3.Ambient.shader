﻿Shader "CustomShader/3.Ambient" {
	Properties {
		_ambient("Ambient", Float) = 0.1
	}
	SubShader {
		Tags {
			"RenderType" = "Opaque"
		}

		Pass {
			CGPROGRAM
			#pragma vertex Vert
			#pragma fragment Frag
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			
			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
			};

			v2f Vert(appdata input) {
				v2f output;
				output.vertex = UnityObjectToClipPos(input.vertex);
				output.normal = UnityObjectToWorldNormal(input.normal);
				return output;
			}

			float _ambient;

			fixed4 Lambert(float3 normal) {
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float lightIntensity = saturate(dot(normal, lightDirection));

				return saturate(_LightColor0 * lightIntensity);
			}

			fixed4 Ambient(float intensity) {
				return saturate(_LightColor0 * intensity);
			}

			fixed4 Frag(v2f input) : SV_Target {
				float3 normal = normalize(input.normal);

				return Lambert(normal) + Ambient(_ambient);
			}
			ENDCG
		}
	}
}
