﻿Shader "CustomShader/7.BumpMapping" {
	Properties {
		_ambient("Ambient", Float) = 0.1
		_shininess("Shininess", Float) = 100
		_bias("Bias", Range(0, 8)) = 1
		_texture("Texture", 2D) = "white" {}
		_cubeMap("Cube Map", Cube) = "white" {}
		_bumpMap("Bump Map", 2D) = "white" {}
	}
	SubShader {
		Tags {
			"RenderType" = "Opaque"
		}

		Pass {
			CGPROGRAM
			#pragma vertex Vert
			#pragma fragment Frag
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			
			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 tangent : TANGENT;
				float2 texcoord : TEXCOORD;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
				float2 texcoord : TEXCOORD0;
				float3 viewDirection : TEXCOORD1;
				float3 tspace0 : TEXCOORD2;
				float3 tspace1 : TEXCOORD3;
				float3 tspace2 : TEXCOORD4;
			};

			float _ambient;
			float _shininess;
			float _bias;
			sampler2D _texture;
			samplerCUBE _cubeMap;
			sampler2D _bumpMap;
			
			fixed4 Lambert(float3 normal) {
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float lightIntensity = saturate(dot(normal, lightDirection));

				return saturate(_LightColor0 * lightIntensity);
			}

			fixed4 Ambient(float intensity) {
				return saturate(_LightColor0 * intensity);
			}

			fixed4 Specular(float3 normal, float3 viewDirection) {
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float3 v = normalize(viewDirection);
				float3 r = reflect(lightDirection, normal);
				float specularIntensity = pow(saturate(dot(v, r)), _shininess);

				return saturate(_LightColor0 * specularIntensity);
			}

			fixed4 Reflection(samplerCUBE cubeMap, float3 viewDirection, float3 normal) {
				return texCUBElod(cubeMap, float4(reflect(viewDirection, normal), _bias));
			}

			v2f Vert(appdata input) {
				v2f output;
				output.vertex = UnityObjectToClipPos(input.vertex);
				output.normal = UnityObjectToWorldNormal(input.normal);
				output.texcoord = input.texcoord;
				output.viewDirection = mul(unity_ObjectToWorld, input.vertex).xyz - _WorldSpaceCameraPos;

				float3 tangent = UnityObjectToWorldDir(input.tangent.xyz);
				float tangentSign = input.tangent.w * unity_WorldTransformParams.w;
				float3 bitangent = cross(output.normal, tangent) * tangentSign;
				output.tspace0 = float3(tangent.x, bitangent.x, output.normal.x);
				output.tspace1 = float3(tangent.y, bitangent.y, output.normal.y);
				output.tspace2 = float3(tangent.z, bitangent.z, output.normal.z);

				return output;
			}

			fixed4 Frag(v2f input) : SV_Target {
				float3 normal;
				float3 normalMap = UnpackNormal(tex2D(_bumpMap, input.texcoord));
                normal.x = dot(input.tspace0, normalMap);
                normal.y = dot(input.tspace1, normalMap);
                normal.z = dot(input.tspace2, normalMap);
                normal = normalize(normal);
                
				fixed4 color = tex2D(_texture, input.texcoord) * Reflection(_cubeMap, input.viewDirection, normal);
				fixed4 light = Lambert(normal) + Ambient(_ambient) + Specular(normal, input.viewDirection);
				
				return color * light;
			}
			ENDCG
		}
	}
}
