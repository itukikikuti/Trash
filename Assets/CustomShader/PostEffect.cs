﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomShader
{
    [ExecuteInEditMode]
    public class PostEffect : MonoBehaviour
    {
        public Material material;

        void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            Graphics.Blit(source, destination, material);
        }
    }
}
