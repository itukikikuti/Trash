﻿Shader "CustomShader/15.CastShadow" {
	Properties {
	}
	SubShader {
		Pass {
			Tags {
				"LightMode" = "ForwardBase"
			}
			CGPROGRAM
			#pragma vertex Vert
			#pragma fragment Frag
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			
			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
			};

			v2f Vert(appdata input) {
				v2f output;
				output.vertex = UnityObjectToClipPos(input.vertex);
				output.normal = UnityObjectToWorldNormal(input.normal);
				return output;
			}

			fixed4 Lambert(float3 normal) {
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float lightIntensity = saturate(dot(normal, lightDirection));

				return saturate(_LightColor0 * lightIntensity);
			}

			fixed4 Frag(v2f input) : SV_Target {
				float3 normal = normalize(input.normal);

				return Lambert(normal);
			}
			ENDCG
		}

		Pass {
			Tags {
				"LightMode" = "ShadowCaster"
			}

			CGPROGRAM
			#pragma vertex Vert
			#pragma fragment Frag
			#pragma multi_compile_shadowcaster
			#include "UnityCG.cginc"
			
			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
			};

			v2f Vert(appdata input) {
				v2f output;
				output.vertex = UnityClipSpaceShadowCasterPos(input.vertex, input.normal);
				output.vertex = UnityApplyLinearShadowBias(output.vertex);
				return output;
			}

			float4 Frag(v2f input) : SV_Target {
				return 0;
			}
			ENDCG
		}
	}
}
