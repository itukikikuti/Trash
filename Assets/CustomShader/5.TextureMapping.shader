﻿Shader "CustomShader/5.TextureMapping" {
	Properties {
		_ambient("Ambient", Float) = 0.1
		_shininess("Shininess", Float) = 100
		_texture("Texture", 2D) = "white" {}
	}
	SubShader {
		Tags {
			"RenderType" = "Opaque"
		}

		Pass {
			CGPROGRAM
			#pragma vertex Vert
			#pragma fragment Frag
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			
			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 texcoord : TEXCOORD;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
				float2 texcoord : TEXCOORD;
				float3 viewDirection : TEXCOORD1;
			};

			float _ambient;
			float _shininess;
			sampler2D _texture;
			
			fixed4 Lambert(float3 normal) {
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float lightIntensity = saturate(dot(normal, lightDirection));

				return saturate(_LightColor0 * lightIntensity);
			}

			fixed4 Ambient(float intensity) {
				return saturate(_LightColor0 * intensity);
			}

			fixed4 Specular(float3 normal, float3 viewDirection) {
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float3 v = normalize(viewDirection);
				float3 r = reflect(lightDirection, normal);
				float specularIntensity = pow(saturate(dot(v, r)), _shininess);

				return saturate(_LightColor0 * specularIntensity);
			}

			v2f Vert(appdata input) {
				v2f output;
				output.vertex = UnityObjectToClipPos(input.vertex);
				output.normal = UnityObjectToWorldNormal(input.normal);
				output.texcoord = input.texcoord;
				output.viewDirection = mul(unity_ObjectToWorld, input.vertex).xyz - _WorldSpaceCameraPos;
				return output;
			}

			fixed4 Frag(v2f input) : SV_Target {
				float3 normal = normalize(input.normal);

				fixed4 color = tex2D(_texture, input.texcoord);
				fixed4 light = Lambert(normal) + Ambient(_ambient) + Specular(normal, input.viewDirection);
				
				return color * light;
			}
			ENDCG
		}
	}
}
