﻿Shader "CustomShader/4.Specular" {
	Properties {
		_ambient("Ambient", Float) = 0.1
		_shininess("Shininess", Float) = 100
	}
	SubShader {
		Tags {
			"RenderType" = "Opaque"
		}

		Pass {
			CGPROGRAM
			#pragma vertex Vert
			#pragma fragment Frag
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			
			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float3 normal : NORMAL;
				float3 viewDirection : TEXCOORD1;
			};

			float _ambient;
			float _shininess;
			
			fixed4 Lambert(float3 normal) {
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float lightIntensity = saturate(dot(normal, lightDirection));

				return saturate(_LightColor0 * lightIntensity);
			}

			fixed4 Ambient(float intensity) {
				return saturate(_LightColor0 * intensity);
			}

			fixed4 Specular(float3 normal, float3 viewDirection) {
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float3 v = normalize(viewDirection);
//				float3 r = reflect(lightDirection, normal);
//				float specularIntensity = pow(saturate(dot(v, r)), _shininess);

				float3 l = -normalize(lightDirection);
				float3 r = -v + (2 * normal * dot(normal, v));
				float specularIntensity = pow(saturate(dot(l, r)), _shininess);
				
				return saturate(_LightColor0 * specularIntensity);
			}

			v2f Vert(appdata input) {
				v2f output;
				output.vertex = UnityObjectToClipPos(input.vertex);
				output.normal = UnityObjectToWorldNormal(input.normal);
				output.viewDirection = mul(unity_ObjectToWorld, input.vertex).xyz - _WorldSpaceCameraPos;
				return output;
			}

			fixed4 Frag(v2f input) : SV_Target {
				float3 normal = normalize(input.normal);

				return Lambert(normal) + Ambient(_ambient) + Specular(normal, input.viewDirection);
			}
			ENDCG
		}
	}
}
