﻿Shader "CustomShader/1.Color" {
	Properties {
	}
	SubShader {
		Tags {
			"RenderType" = "Opaque"
		}

		Pass {
			CGPROGRAM
			#pragma vertex Vert
			#pragma fragment Frag
			#include "UnityCG.cginc"

			float4 Vert(float4 vertex : POSITION) : SV_POSITION {
				return UnityObjectToClipPos(vertex);
			}

			fixed4 Frag() : SV_Target {
				return fixed4(0.5, 0.5, 0.5, 1);
			}
			ENDCG
		}
	}
}
