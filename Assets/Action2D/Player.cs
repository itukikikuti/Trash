﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Action2D
{
    public class Player : Character
    {
        public Sprite landingSprite;
        public Sprite[] walkSprite;
        public Sprite jumpSprite;

        float spriteIndex = 0f;

        void FixedUpdate()
        {
            Camera.main.transform.position = new Vector3(transform.position.x + 5f, 4f, -10f);

            float speed = walkSpeed;
            if (Input.GetKey(KeyCode.X))
            {
                speed = dashSpeed;
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                Move(-speed);
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                Move(speed);
            }

            if (Input.GetKeyDown(KeyCode.Z))
            {
                Jump();
            }

            rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x * friction, rigidbody2d.velocity.y);

            if (Input.GetKeyUp(KeyCode.Z))
            {
                if (rigidbody2d.velocity.y > 0f)
                {
                    rigidbody2d.velocity -= new Vector2(0f, rigidbody2d.velocity.y / 2f);
                }
            }

            Draw();
        }

        void Draw()
        {
            if (isLanding)
            {
                if (-1f < rigidbody2d.velocity.x && rigidbody2d.velocity.x < 1f)
                {
                    spriteRenderer.sprite = landingSprite;
                    return;
                }
                spriteIndex += Time.deltaTime * Mathf.Abs(rigidbody2d.velocity.x) * 3f;
                spriteIndex %= 3f;
                spriteRenderer.sprite = walkSprite[Mathf.FloorToInt(spriteIndex)];
            }
            else
            {
                spriteRenderer.sprite = jumpSprite;
            }
        }
    }
}
