﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Action2D
{
    public class Enemy : Character
    {
        protected override void OnCollision(Collision2D collision)
        {
            base.OnCollision(collision);

            for (int i = 0; i < collision.contacts.Length; i++)
            {
                ContactPoint2D contact = collision.contacts[i];

                if (contact.collider.tag != "Player")
                {
                    continue;
                }

                Player player = contact.collider.GetComponent<Player>();

                if (transform.position.y < player.transform.position.y &&
                    player.rigidbody2d.velocity.y < 0f)
                {
                    player.rigidbody2d.velocity += new Vector2(0f, 5f);
                    Dead();
                    return;
                }
                else
                {
                    player.Dead();
                }
            }
        }
    }
}
