﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Action2D
{
    public class Character : MonoBehaviour
    {
        public Rigidbody2D rigidbody2d { get; protected set;}
        public SpriteRenderer spriteRenderer { get; protected set;}

        public float walkSpeed = 1f;
        public float dashSpeed = 3f;
        public float friction = 0.75f;
        public float jumpPower = 16f;
        protected bool isLanding = false;

        void Awake()
        {
            rigidbody2d = GetComponent<Rigidbody2D>();
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void Move(float speed)
        {
            rigidbody2d.velocity += new Vector2(speed, 0f);

            if (rigidbody2d.velocity.x > 0f)
            {
                spriteRenderer.flipX = false;
            }
            else
            {
                spriteRenderer.flipX = true;
            }
        }

        public void Jump()
        {
            if (isLanding)
            {
                rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x, jumpPower);
            }
        }

        public void Dead()
        {
            Collider2D[] colliders = GetComponents<Collider2D>();
            for (int i = 0; i < colliders.Length; i++)
            {
                colliders[i].enabled = false;
            }

            rigidbody2d.constraints = RigidbodyConstraints2D.None;
            rigidbody2d.velocity = new Vector2(Random.Range(-10f, 10f), 10f);
            rigidbody2d.angularVelocity = Random.Range(-500f, 500f);

            Destroy(this);
            Destroy(gameObject, 3f);
        }

        protected virtual void OnCollision(Collision2D collision)
        {
            isLanding = false;
            for (int i = 0; i < collision.contacts.Length; i++)
            {
                if (collision.contacts[i].normal.y > 0.5f)
                {
                    isLanding = true;
                    return;
                }
            }
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            OnCollision(collision);
        }

        void OnCollisionStay2D(Collision2D collision)
        {
            OnCollision(collision);
        }

        void OnCollisionExit2D(Collision2D collision)
        {
            OnCollision(collision);
        }
    }
}
