﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Action2D
{
    public class Enemy2 : Enemy
    {
        float direction = -1f;

        void FixedUpdate()
        {
            Move(walkSpeed * direction);

            rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x * friction, rigidbody2d.velocity.y);
        }

        protected override void OnCollision(Collision2D collision)
        {
            base.OnCollision(collision);

            for (int i = 0; i < collision.contacts.Length; i++)
            {
                ContactPoint2D contact = collision.contacts[i];
                if (contact.normal.x < -0.5f)
                {
                    direction = -1f;
                }

                if (contact.normal.x > 0.5f)
                {
                    direction = 1f;
                }
            }
        }
    }
}
