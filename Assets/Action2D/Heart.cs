﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Action2D
{
    public class Heart : Character
    {
        static Tilemap tilemap;

        public TileBase tile;
        BoxCollider2D boxCollider;
        bool isActive = false;

        void Start()
        {
            spriteRenderer.sortingOrder = -1;
            boxCollider = GetComponents<BoxCollider2D>()[0];
            boxCollider.enabled = false;
            rigidbody2d.bodyType = RigidbodyType2D.Kinematic;
        }

        void FixedUpdate()
        {
            if (!isActive)
            {
                return;
            }

            Move(walkSpeed);

            if (isLanding)
            {
                Jump();
            }

            rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x * friction, rigidbody2d.velocity.y);
        }

        IEnumerator OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.tag != "Player")
            {
                yield break;
            }

            if (isActive)
            {
                spriteRenderer.enabled = false;

                Time.timeScale = 0f;
                yield return new WaitForSecondsRealtime(1f);
                Time.timeScale = 1f;

                Destroy(gameObject);
            }
            else
            {
                if (transform.position.y < collider.transform.position.y)
                {
                    yield break;
                }

                if (collider.attachedRigidbody.velocity.y < 1f)
                {
                    yield break;
                }

                if (tilemap == null)
                {
                    tilemap = GetComponentInParent<Tilemap>();
                }

                Vector3Int position = new Vector3Int();
                position.x = Mathf.FloorToInt(transform.position.x);
                position.y = Mathf.FloorToInt(transform.position.y);
                tilemap.SetTile(position, tile);

                for (int i = 0; i < 30; i++)
                {
                    transform.position += new Vector3(0f, 1f / 30f);
                    yield return null;
                }

                isActive = true;
                boxCollider.enabled = true;
                rigidbody2d.bodyType = RigidbodyType2D.Dynamic;
            }
        }
    }
}
