﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shooter2D
{
    public class Bullet : MonoBehaviour
    {
        public GameObject hitEffect;
        public float speed;
        public AudioSource sound;

        IEnumerator Start()
        {
            transform.position += transform.right;
            sound.Play();
            yield return new WaitForSeconds(3f);
            Destroy(gameObject);
        }

        void Update()
        {
            transform.position += transform.right * speed;
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.tag != "Bullet")
            {
                Instantiate(hitEffect, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
}
