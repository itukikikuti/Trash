﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shooter2D
{
    [System.Serializable]
    public class Gun
    {
        public GameObject bullet;
        public Vector3 position;
        public float angle;

        public void Fire(Vector3 origin, bool isAimPlayer = false)
        {
            if (isAimPlayer)
            {
                GameObject player = GameObject.FindWithTag("Player");
                angle = Mathf.Atan2(player.transform.position.y - (origin + position).y, player.transform.position.x - (origin + position).x) * Mathf.Rad2Deg;

            }
            GameObject.Instantiate(bullet, origin + position, Quaternion.Euler(0f, 0f, angle));
        }
    }
}
