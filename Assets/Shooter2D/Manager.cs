﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Shooter2D
{
    public class Manager : MonoBehaviour
    {
        void Awake()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                GameObject enemy = transform.GetChild(i).gameObject;
                enemy.SetActive(false);
            }
        }

        void Start()
        {
            StartCoroutine(ManageEnemy());
            StartCoroutine(ManagePlayer());
        }

        IEnumerator ManageEnemy()
        {
            float interval = 2f;

            while (transform.childCount > 0)
            {
                int otherChildCount = transform.childCount;

                for (int i = 0; i < transform.childCount; i++)
                {
                    GameObject enemy = transform.GetChild(i).gameObject;

                    if (enemy.transform.localPosition.y < interval)
                    {
                        enemy.SetActive(true);
                        otherChildCount--;
                    }
                    enemy.transform.position -= new Vector3(0f, interval, 0f);
                }
                yield return new WaitUntil(() => transform.childCount <= otherChildCount);
            }
            SceneManager.LoadScene("Shooter2D/Title");
        }

        IEnumerator ManagePlayer()
        {
            GameObject player = GameObject.FindWithTag("Player");
            yield return new WaitUntil(() => player == null);
            SceneManager.LoadScene("Shooter2D/Title");
        }
    }
}
