﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Shooter2D
{
    public class Player : MonoBehaviour
    {
        public Gun gun;
        public int life;

        IEnumerator Start()
        {
            while (true)
            {
                yield return new WaitUntil(() => Input.GetMouseButton(0));
                gun.Fire(transform.position);
                yield return new WaitForSeconds(0.25f);
            }
        }

        void Update()
        {
            Vector3 position = Input.mousePosition;
            position.z = -Camera.main.transform.position.z;

            position = Camera.main.ScreenToWorldPoint(position);

            position.x = Mathf.Clamp(position.x, -2.5f, 2.5f);
            position.y = transform.position.y;

            transform.position = position;
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.tag == "Bullet")
            {
                life--;
                if (life == 0)
                {
                    transform.eulerAngles = new Vector3(0f, 0f, -90f);
                    Destroy(this);
                    Destroy(gameObject, 1f);
                }
            }
        }
    }
}
