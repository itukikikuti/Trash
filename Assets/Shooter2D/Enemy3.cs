﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shooter2D
{
    public class Enemy3 : Enemy
    {
        IEnumerator Start()
        {
            yield return Move(new Vector2(0f, -0.1f), 1f);
            gun.Fire(transform.position, true);

            yield return Move(new Vector2(0f, -0.1f), 1f);
            gun.Fire(transform.position, true);

            yield return Move(new Vector2(0f, -0.1f), 2f);
            Destroy(gameObject);
        }
    }
}
