﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shooter2D
{
    public class Enemy1 : Enemy
    {
        IEnumerator Start()
        {
            yield return Move(new Vector2(0f, -0.1f), 1f);

            while (true)
            {
                yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
                gun.Fire(transform.position, true);
            }
        }
    }
}
