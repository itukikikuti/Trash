﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Shooter2D
{
    public class Title : MonoBehaviour
    {
        void Update()
        {
            if (Input.GetMouseButtonUp(0))
            {
                SceneManager.LoadScene("Shooter2D/Main");
            }
        }
    }
}
