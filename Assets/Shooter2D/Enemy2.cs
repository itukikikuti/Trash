﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shooter2D
{
    public class Enemy2 : Enemy
    {
        IEnumerator Start()
        {
            yield return Move(new Vector2(0f, -0.1f), 1f);
            yield return new WaitForSeconds(0.5f);

            while (true)
            {
                yield return Move(new Vector2(0.05f, 0f), 1f);
                gun.Fire(transform.position, true);
                yield return new WaitForSeconds(Random.Range(0.5f, 1f));

                yield return Move(new Vector2(-0.05f, 0f), 1f);
                gun.Fire(transform.position, true);
                yield return new WaitForSeconds(Random.Range(0.5f, 1f));

                yield return Move(new Vector2(-0.05f, 0f), 1f);
                gun.Fire(transform.position, true);
                yield return new WaitForSeconds(Random.Range(0.5f, 1f));

                yield return Move(new Vector2(0.05f, 0f), 1f);
                gun.Fire(transform.position, true);
                yield return new WaitForSeconds(Random.Range(0.5f, 1f));
            }
        }
    }
}
