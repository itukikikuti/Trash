﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shooter2D
{
    public class Enemy : MonoBehaviour
    {
        public Gun gun;
        public int life;

        public IEnumerator Move(Vector2 velocity, float time)
        {
            for (int i = 0; i < time / Time.fixedDeltaTime; i++)
            {
                transform.position += new Vector3(velocity.x, velocity.y, 0f);
                yield return null;
            }
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.tag == "Bullet")
            {
                life--;
                if (life == 0)
                {
                    transform.eulerAngles = new Vector3(0f, 0f, -90f);
                    Destroy(this);
                    Destroy(gameObject, 1f);
                }
            }
        }
    }
}
