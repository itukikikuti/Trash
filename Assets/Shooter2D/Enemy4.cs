﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shooter2D
{
    public class Enemy4 : Enemy
    {
        IEnumerator Start()
        {
            yield return Move(new Vector2(0f, -0.1f), 1f);

            while (true)
            {
                for (int i = 0; i < 5; i++)
                {
                    yield return new WaitForSeconds(0.1f);
                    gun.Fire(transform.position, true);
                }
                yield return new WaitForSeconds(Random.Range(1f, 1.5f));
            }
        }
    }
}
