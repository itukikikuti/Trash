﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Run2D
{
    public class Title : MonoBehaviour
    {
        TextMesh text;

        void Start()
        {
            text = GetComponent<TextMesh>();
            text.text = "ハイスコア " + PlayerPrefs.GetFloat("Score", 0f).ToString("#,0") + "m";
        }

        void Update()
        {
            if (Input.GetKeyUp(KeyCode.Return))
            {
                SceneManager.LoadScene("Run2D/Main");
            }
        }
    }
}
