﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Run2D
{
    public class Stage : MonoBehaviour
    {
        public int length;
        public GameObject ground;
        public GameObject[] hurdle;
        Transform grounds;
        Transform hurdles;
        AudioSource music;
        Player player;

        void Start()
        {
            grounds = new GameObject("Grounds").transform;
            grounds.parent = transform;
            grounds.transform.localPosition = new Vector3(0f, 0f, 0f);

            hurdles = new GameObject("Hurdles").transform;
            hurdles.parent = transform;
            hurdles.transform.localPosition = new Vector3(0f, 1f, 0f);

            music = GetComponent<AudioSource>();

            player = GameObject.FindWithTag("Player").GetComponent<Player>();

            for (int i = 0; i < length * 2; i++)
            {
                GameObject obj = Instantiate(ground, grounds);
                obj.transform.localPosition = new Vector3(-length + i, 0f, 0f);
            }

            for (int i = 0; i < hurdle.Length; i++)
            {
                GameObject obj = Instantiate(hurdle[i], hurdles);
                obj.transform.localPosition = new Vector3(-length, 0f, 0f);
            }
        }

        void Update()
        {
            music.pitch = Mathf.Lerp(1f, 1.5f, Mathf.InverseLerp(0.1f, 0.5f, player.Speed));

            for (int i = 0; i < grounds.childCount; i++)
            {
                grounds.GetChild(i).localPosition -= new Vector3(player.Speed, 0f, 0f);

                if (grounds.GetChild(i).localPosition.x < -length)
                {
                    grounds.GetChild(i).localPosition += new Vector3(length * 2, 0f, 0f);
                }
            }

            for (int i = 0; i < hurdles.childCount; i++)
            {
                hurdles.GetChild(i).localPosition -= new Vector3(player.Speed, 0f, 0f);

                if (hurdles.GetChild(i).localPosition.x < -length && Random.value <= 0.01f)
                {
                    hurdles.GetChild(i).localPosition = new Vector3(length, 0f, 0f);
                }
            }
        }
    }
}
