﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Run2D
{
    public class Score : MonoBehaviour
    {
        float score;
        Player player;
        TextMesh text;

        void Start()
        {
            text = GetComponent<TextMesh>();
            player = GameObject.FindWithTag("Player").GetComponent<Player>();
        }

        void Update()
        {
            score += player.Speed;
            text.text = score.ToString("#,0") + "m";
        }

        void OnDestroy()
        {
            if (PlayerPrefs.GetFloat("Score", 0f) < score)
            {
                PlayerPrefs.SetFloat("Score", score);
            }
        }
    }
}
