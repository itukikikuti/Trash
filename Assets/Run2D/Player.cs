﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Run2D
{
    public class Player : MonoBehaviour
    {
        public float jumpPower;
        public float Speed { get; private set; }
        bool isLanding = false;
        Rigidbody2D rigidbody2d;
        SpriteRenderer spriteRenderer;
        AudioSource jumpSound;
        ParticleSystem particle;

        public Sprite[] walkSprite;
        public Sprite jumpSprite;

        float spriteIndex = 0f;

        void Start()
        {
            rigidbody2d = GetComponent<Rigidbody2D>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            jumpSound = GetComponent<AudioSource>();
            particle = GetComponent<ParticleSystem>();
            Speed = 0.1f;
        }

        void Update()
        {
            Speed += 0.0001f;

            if (Speed > 0.5f)
            {
                Speed = 0.5f;
                particle.Emit(1);
            }
            
            if (isLanding)
            {
                spriteIndex += Time.deltaTime * Speed * 50f;
                spriteIndex %= 3f;
                spriteRenderer.sprite = walkSprite[Mathf.FloorToInt(spriteIndex)];

                if (Input.GetKeyDown(KeyCode.Return))
                {
                    rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x, jumpPower);
                    jumpSound.Play();
                }
            }
            else
            {
                spriteRenderer.sprite = jumpSprite;

                if (Input.GetKeyUp(KeyCode.Return))
                {
                    if (rigidbody2d.velocity.y > 0f)
                    {
                        rigidbody2d.velocity -= new Vector2(0f, rigidbody2d.velocity.y / 2f);
                    }
                }
            }
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
            SceneManager.LoadScene("Run2D/Title");
        }

        void OnCollision(Collision2D collision)
        {
            isLanding = false;
            for (int i = 0; i < collision.contacts.Length; i++)
            {
                if (collision.contacts[i].normal.y > 0.5f)
                {
                    isLanding = true;
                    return;
                }
            }
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            OnCollision(collision);
        }

        void OnCollisionStay2D(Collision2D collision)
        {
            OnCollision(collision);
        }

        void OnCollisionExit2D(Collision2D collision)
        {
            OnCollision(collision);
        }
    }
}
