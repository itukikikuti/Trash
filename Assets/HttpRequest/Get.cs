﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;

namespace HttpRepuest
{
    public class Get : MonoBehaviour
    {
        public string url;
        string text;

        IEnumerator Start()
        {
            UnityWebRequest request = UnityWebRequest.Get(url);
            yield return request.SendWebRequest();

            if (request.isNetworkError)
            {
                text = request.error;
                yield break;
            }

            if (request.responseCode == 200)
            {
                text = request.downloadHandler.text;
                text = Regex.Replace(text, @"<[^>]+>", "");
                text = Regex.Replace(text, @"\n{2,}", "\n");
            }
        }

        void OnGUI()
        {
            GUI.contentColor = Color.black;
            GUI.Label(new Rect(0f, 0f, Screen.width, Screen.height), text);
        }
    }
}
