﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Voice
{
    public class WaveAnalyzer : MonoBehaviour
    {
        public int samples = 1024;
        LineRenderer line;

        void Start()
        {
            line = GetComponent<LineRenderer>();
            line.positionCount = samples;
        }

        void Update()
        {
            float[] wave = new float[samples];
            AudioListener.GetOutputData(wave, 0);

            for (int i = 0; i < samples; i++)
            {
                line.SetPosition(i, new Vector3(i / (float)samples, wave[i]));
            }
        }
    }
}
