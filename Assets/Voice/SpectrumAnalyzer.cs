﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Voice
{
    public class SpectrumAnalyzer : MonoBehaviour
    {
        public int samples = 4096;
        LineRenderer line;

        void Start()
        {
            line = GetComponent<LineRenderer>();
            line.positionCount = samples;
        }

        void Update()
        {
            float[] spectrum = new float[samples];
            AudioListener.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);
            string log = "";

            for (int i = 0; i < samples; i++)
            {
                line.SetPosition(i, new Vector3(i / (float)samples, spectrum[i]));

                if (i == 0 || i == samples - 1)
                {
                    continue;
                }

                if (spectrum[i - 1] + 0.005f < spectrum[i] && spectrum[i] > spectrum[i + 1] + 0.005f)
                {
                    log += (22050f / samples * i).ToString("f2") + "Hz," + spectrum[i].ToString("f2") + " ";
                }
            }
            Debug.Log(log);
        }
    }
}
