﻿//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Voice
{
    public class VoiceSynthesizer : MonoBehaviour
    {
        [System.Serializable]
        public class Formant
        {
            public float frequency;
            public float level;
            [HideInInspector]
            public float t;
        }

        public Formant[] formant;
        float samplingRate = 44100f;

        void Awake()
        {
            samplingRate = (float)AudioSettings.outputSampleRate;
        }

        void OnAudioFilterRead(float[] data, int channels)
        {
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = 0f;
            }

            for (int i = 0; i < formant.Length; i++)
            {
                Sin(data, channels, formant[i]);
            }
        }

        void Sin(float[] data, int channels, Formant formant)
        {
            for (int i = 0; i < data.Length; i += channels)
            {
                formant.t += formant.frequency / samplingRate;
                formant.t %= 1f;
                float sample = Mathf.Sin(Mathf.PI * 2f * formant.t) * formant.level;

                for (int j = 0; j < channels; j++)
                {
                    data[i + j] += sample;
                }
            }
        }
    }
}
