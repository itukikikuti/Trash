﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Voice
{
    public class Microphone : MonoBehaviour
    {
        AudioSource audioSource;

        void Start()
        {
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = UnityEngine.Microphone.Start(null, true, 1, 44100);
            audioSource.Play();
        }
    }
}
