﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Voice
{
    public class Test : MonoBehaviour
    {
        public AudioClip a;
        public AudioClip ka;
        public AudioSource audioSource;
        public float quality;

        public int samples = 44100;
        public int vowel = 0;

        void Start()
        {
            CreateVoice();
        }

        void OnValidate()
        {
            if (!Application.isPlaying)
                return;
            
            CreateVoice();
        }

        //        void Start()
        //        {
        //            int lengthSamples = (int)(a.samples * a.channels * a.length);
        //            float[] aData = new float[lengthSamples];
        //            float[] kaData = new float[lengthSamples];
        //            float[] kData = new float[lengthSamples];
        //            a.GetData(aData, 0);
        //            ka.GetData(kaData, 0);
        //
        //            for (int i = 0; i < lengthSamples; i++)
        //            {
        //                kData[i] = Mathf.Round(kaData[i] * quality) / quality;
        //            }
        //
        //            AudioClip k = AudioClip.Create("k", (int)(a.samples * a.length), a.channels, a.frequency, false);
        //            k.SetData(kData, 0);
        //            audioSource.clip = k;
        //            audioSource.Play();
        //        }
        
        float t = 0;
        public AnimationCurve curve;

        float GenRosenberg(int frequency)
        {
            // Rosenberg波を生成
            // 声門開大期
//            float tau1 = 0.9f;
            // 声門閉小期
//            float tau2 = 0.95f;
//            float sample = 0f;

//            Debug.Log(t);

            t += (frequency + Random.Range(-10f, 10f))  / samples;
            t %= 1;
//            return t * 2f - 1f;
            return curve.Evaluate(t);

//            if (t <= tau1)
//            {
//                sample = 3f * Mathf.Pow(t / tau1, 2f) - 2f * Mathf.Pow(t / tau1, 3f);
//            }
//            else if (t < tau1 + tau2)
//            {
//                sample = 1f - Mathf.Pow((t - tau1) / tau2, 2f);
//            }
//            return 2f * (sample - 0.5f);
        }

        float[] SettingReso(float f, float Q)
        {
            // BPFのフィルタ係数を求める
            float omega = 2f * Mathf.PI * f / samples;
            float alpha = Mathf.Sin(omega) / (2f * Q);
            float a0 = 1f + alpha;

            float[] param = new float[5];
            param[0] = alpha / a0;
            param[1] = 0f / a0;
            param[2] = -alpha / a0;
            param[3] = -2f * Mathf.Cos(omega) / a0;
            param[4] = (1f - alpha) / a0;

            return param;
        }

        float ApplyFilter(float b, float[] param, float[] delay)
        {
            // フィルタの適用
            float sample = 0f;
            sample += param[0] * b;
            sample += param[1] * delay[0];
            sample += param[2] * delay[1];
            sample -= param[3] * delay[2];
            sample -= param[4] * delay[3];
            delay[1] = delay[0];
            delay[0] = b;
            delay[3] = delay[2];
            delay[2] = sample;
            return sample;
        }

        // 基本周波数
        public int frequency = 220;
        // 声道の長さ
        public float vocalTract = 15f;
        //        public float[] male = new float[]{ 1.6f, 0.7f, 1.1f, 1f, 1f };

        void CreateVoice()
        {
            if (!Application.isPlaying)
                return;
            
            audioSource.Stop();

            float[] formant = new float[5];
            float[][] param = new float[5][];
            float[][] delay = new float[][]
            {
                new float[4], new float[4], new float[4], new float[4], new float[4]
            };
            float[][] male = new float[][]
            {
                new float[]{ 2f, 0.5f, 1.1f, 1f, 1f },
                new float[]{ 0.7f, 1.4f, 1.2f, 1f, 1f },
                new float[]{ 0.8f, 0.9f, 0.9f, 1f, 1f },
                new float[]{ 1.2f, 1.3f, 1.1f, 1f, 1f },
                new float[]{ 1.15f, 0.5f, 1.2f, 1f, 1f },
            };

            // 1秒間
            int size = samples * 1;
            float[] buffer = new float[size];

            // 共鳴周波数を計算
            for (int i = 0; i < 5; i++)
            {
                formant[i] = (34000 * (2 * i + 1)) / (4 * vocalTract);
                formant[i] *= male[vowel][i];
            }

            // 波形の生成
            for (int i = 0; i < size; i++)
            {
                float output = 0f;
                float input = GenRosenberg(frequency);

                for (int j = 0; j < 5; j++)
                {
                    param[j] = SettingReso(formant[j], 20f);
                    output += ApplyFilter(input, param[j], delay[j]);
                }
//                buffer[i] = input;
                buffer[i] = output;
//                buffer[i] = (char)(128.0 * MIN(MAX(output, -1.0), 1.0)) + 128;
            }

            AudioClip clip = AudioClip.Create("voice", size, 1, samples, false);
            clip.SetData(buffer, 0);
            audioSource.clip = clip;
            audioSource.Play();
        }
    }
}
